# NFZ - Time kills pain
Projekt ten ma w założeniu służyć jako automatyczny sprawdzacz
dostępności wybranych wizyt w NFZ korzystając z publicznych informacji
pobranych z [tej](https://terminyleczenia.nfz.gov.pl/)
strony. Sprawdzanie ma się odbywać z podaną częstotliwością (np. co 5
minut) i jeżeli uda się znaleźć wolny termin, wysyłana jest informacja
do użytkownika przez SMS bądź email.

## Krótki raport
### Co zostało zrobione
Oto rzeczy, które udało nam się skończyć:

* Szkielet GUI
* Obsługa komunikacji z serwerami NFZu

### Co nie zostało zrobione
Tych spraw niestety nie udało się załatwić przed deadlinem:

* Obsługa wysyłania powiadomień na SMS i email
* Działanie programu w tle

### Co chcemy poprawić
A oto lista rzeczy, nad którymi chcemy jeszcze popracować:

* Dokończenie GUI (zrobienie reszty okienek dialogowych)
* Uatrakcyjnienie szaty graficznej

## Intrukcja obsługi
### Czynności wstępne
Co trzeba mieć, żeby odpalić:

1. Należy posiadać **Pythona** w wersji 3.5 lub nowszej
2. Należy zainstalować następujące niestandardowe moduły Pythona:
   **requests**, **lxml** oraz **PyQt5**
3. Należy posiadać połączenie internetowe

### Jak korzystać
Instrukcja obsługi:

1. Uruchamiamy plik: *nfz/gui/mainwindow.py*
2. Wybieramy z menu *Wizyty → Dodaj wizytę → NFZ*
3. Wprowadzamy dane
4. Wybieramy z menu *Plik → Zapisz*
5. Wyłączamy program
