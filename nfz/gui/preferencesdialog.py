from PyQt5.QtWidgets import *


class NFZPreferences(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.data = {}

        # Inputy
        self.email_input = QLineEdit(self)
        self.tel_input = QLineEdit(self)
        self.interval_input = QSpinBox(self)
        self.interval_input.setRange(1, 60)
        self.interval_input.setSuffix('min')
        self.interval_input.setSingleStep(1)
        self.interval_input.setValue(5)

        # Buttony
        self.cancel = QPushButton('Anuluj', self)
        self.ok = QPushButton('Ok', self)
        self.cancel.clicked.connect(self.reject)
        self.ok.clicked.connect(self.get_data)

        # Layouty
        form = QFormLayout()
        form.addRow('Email', self.email_input)
        form.addRow('Telefon', self.tel_input)
        form.addRow('Interwał', self.interval_input)

        hbox = QHBoxLayout()
        hbox.addStretch()
        hbox.addWidget(self.cancel)
        hbox.addWidget(self.ok)

        vbox = QVBoxLayout(self)
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        vbox.addStretch()

        self.setLayout(vbox)
        self.setWindowTitle('Ustawienia')
        self.show()

        self.setFixedSize(600, self.height())


    def get_data(self):
        self.data['email'] = self.email_input.text()
        self.data['telephone'] = self.tel_input.text()
        self.data['interval'] = self.interval_input.value()
        self.accept()
        
if __name__ == '__main__':
    app = QApplication([])
    p = NFZPreferencesDialog()
    app.exec_()
