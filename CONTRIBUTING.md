# NFZ - Time kills pain
Witam serdecznie, w tymże pliczku są przedstawione zasady
współpracy. Liczę na respektowanie zasad oraz pozytywne skutki naszego
projektu.

---

## Kod
Konwencje podczas pisania kodu:

* Piszemy w **Pythonie** ver. 3.5 lub wyższej
* Staramy się stosować do zasad przedstawionych w [tym
  dokumencie](https://www.python.org/dev/peps/pep-0008/ "PEP8")
* **Docstringi** i **dokumentację** piszemy po polsku natomiast resztę po
  angielsku
* *Keep it simple, stupid*

## Zachowanie
Kultura współpracy:

* Konflikty rozwiązujemy przez debatę
* Krytykujemy kod, nie jego twórcę
* Nie bierzemy spraw personalnie

---

Tak ma być właśnie.

~bruteus
