import requests
from lxml import html
from json import loads

LOGON_URL = 'https://mol.medicover.pl/Users/Account/LogOn'
LOGOFF_URL = 'https://mol.medicover.pl/Users/Account/LogOff'
AUTH_URL = 'https://mol.medicover.pl/Medicover.OpenIdConnectAuthentication/Account/OAuthSignIn'

class MedicoverRequest:
    def __init__(self, username, password):
        self.session = requests.Session()

        result = self.session.get(LOGON_URL)
        logon_html = html.fromstring(result.text)
        modelJson = loads(logon_html.cssselect('#modelJson')[0].text.replace('&quot;', '"'))
        token = modelJson['antiForgery']['value']

        logon_payload = {
            'username': username,
            'password': password,
            'idsrv.xsrf': token
        }

        result = self.session.post(result.url, data = logon_payload)
        result.raise_for_status()

        auth_html = html.fromstring(result.text)
        get_property = lambda name: \
            auth_html.cssselect('input[name="%s"]' % name)[0].value

        auth_payload = {
            'code': get_property('code'),
            'id_token': get_property('id_token'),
            'scope': get_property('scope'),
            'state': get_property('state'),
            'session_state': get_property('session_state')
        }

        result = self.session.post(AUTH_URL, data = auth_payload)
        result.raise_for_status()


    def logoff(self):
        self.session.get(LOGOFF_URL)
