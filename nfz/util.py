BASE_URL = 'https://terminyleczenia.nfz.gov.pl/'

STATES = [
    'dolnośląskie', 'kujawsko-pomorskie', 'lubelskie', 'lubuskie',
    'łódzkie', 'małopolskie', 'mazowieckie', 'opolskie','podkarpackie',
    'podlaskie', 'pomorskie','śląskie', 'świętokrzyskie',
    'warmińsko-mazurskie', 'wielkopolskie', 'zachodniopomorskie'
]

def get_state_id(state):
    """Zamienia `state` na odpowiedni numer województwa

    Jeżeli `state` jest równe '', funkcja zwraca ''.
    W przeciwnym wypadku zwracany jest numer indeksu na podstawie
    listy `STATES` w formacie liczby dwucyfrowej dopełnionej zerami
    z lewej strony, jeżeli jest taka potrzeba.
    """
    if state not in STATES and state != '':
        raise ValueError('`state` should be one of the following: ' +
                         '{} or \'\''.format(STATES))

    if state == '':
        return ''
    else:
        return '{:02}'.format(STATES.index(state) + 1)
