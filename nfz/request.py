from urllib.parse import quote  # do formatowania URL
from urllib.request import urlopen
import lxml.html  # do parsowania HTML
from json import dumps  # do JSONa
from util import get_state_id, BASE_URL

class NFZRequest:
    def __init__(self, case, locality, place, provider, service_name, \
                 state, street):
        """Inicjacja wszystkich parametrów do wysłania requesta

        Opis parametrów:
        `case`: '1' lub '2', gdzie '1' oznacza przypadek stabilny, a '2' - pilny
        `locality`: miejscowość
        `place`: miejsce udzielania świadczeń
        `provider`: szpital/przychodnia
        `service_name`: szukane świadczenie
        `state`: województwo
        `street`: ulica

        Wszystkie parametry powinny być typu `str`.
        Jeżeli nie posiadamy danych dla jakiegoś z parametrów, w jego miejsce
        wstawiamy: ''.
        Do poprawnego utworzenia requestu konieczne będą parametry `case` oraz
        `service_type`.

        Zalecane jest, aby wartości wszystkich parametrów były wyciągnięte
        z serwera NFZ poprzez autokompletowanie w formularzu, w celu
        osiągnięcia najlepszych wyników wyszukiwania.
        """
        # Sprawdzanie poprawności parametrów
        if case not in ['1', '2']:
            raise ValueError('`case` should be \'1\' or \'2\'')
        for parameter in [locality, place, provider, service_name, state, street]:
            if type(parameter) != str:
                raise TypeError('Non-string parameter found: {}'.format(parameter))

        # Przypisanie
        self.query_parameters = {
            'case': case,
            'locality': locality,
            'place': place,
            'provider': provider,
            'service_name': service_name,
            'state': get_state_id(state),
            'street': street
        }


    @classmethod
    def from_dict(cls, d):
        """Tworzy nowy obiekt ze słownika

        Jeżeli podany słownik nie zawiera wszystkich potrzebnych kluczy,
        rzucany jest wyjątek.
        """
        needed_keys = ['case', 'locality', 'place', 'provider', \
                       'service_name', 'state', 'street']
        for k in needed_keys:
            if k not in d.keys():
                raise ValueError('Dict hasn\'t got all keys needed')

        if d['case'] == 'pilny':
            case = '1'
        else:
            case = '2'
        return cls(case, d['locality'], d['place'], d['provider'], \
                   d['service_name'], d['state'], d['street'])


    def form_url(self):
        """Zwraca stworzony z `_BASE_URL` i `query_parameters` adres URL"""
        query = '?search=true'
        for k, v in self.query_parameters.items():
            query += '&{}={}'.format(k, quote(v))
        return BASE_URL + query


    def send_request(self):
        """Wysyła request na URL pobrany z `form_url` i zapisuje rezultat jako string"""
        self.query_result = urlopen(self.form_url()).read().decode('utf-8')


    def extract_data(self):
        """Wyciąga interesujące dane z `query_result` i zapisuje je w JSONie"""
        html = lxml.html.fromstring(self.query_result)
        results_html = html.cssselect('div.result-record')
        self.results_json = []

        for rh in results_html:
            result = {}
            result['świadczenie'] = self.extract_swiadczenie(rh)
            result['placówka'] = self.extract_placowka(rh)
            result['adres'] = self.extract_adres(rh)
            result['telefon'] = self.extract_telefon(rh)
            result['udogodnienia'] = self.extract_udogodnienia(rh)
            result['pierwszy_wolny_termin'] = self.extract_termin(rh)
            result['liczba_osób_oczekujących'] = self.extract_oczekujacy(rh)
            result['liczba_osób_skreślonych'] = self.extract_skresleni(rh)
            result['średni_czas_oczekiwania'] = self.extract_czas(rh)
            self.results_json.append(dumps(result, indent = 3, ensure_ascii = False))


    # Metody do ekstrakcji poszczególnych informacji
    def extract_swiadczenie(self, el):
        return el.cssselect('p.range-name')[0].text_content().split(':', 1)[1].strip()

    def extract_placowka(self, el):
        return el.cssselect('p.swd-name')[0].text_content().split(':', 1)[1].strip()

    def extract_adres(self, el):
        return el.cssselect('address p')[0].text_content().split(':', 1)[1].strip()

    def extract_telefon(self, el):
        return el.cssselect('address p')[1].text_content().split(':', 1)[1].strip()

    def extract_udogodnienia(self, el):
        ud = [i.text_content() \
              for i in el.cssselect('div.facilities div.myTableCell span.visuallyhidden')]
        return list(set(ud))

    def extract_termin(self, el):
        return el.cssselect('p.result-date')[0].text_content().strip()

    def extract_oczekujacy(self, el):
        return el.cssselect('div.date span')[0].text_content().strip()

    def extract_skresleni(self, el):
        return el.cssselect('div.date span')[1].text_content().strip()

    def extract_czas(self, el):
        return el.cssselect('div.date span')[2].text_content().strip()
