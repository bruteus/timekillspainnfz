import sys
sys.path.append('..')
from util import STATES
from autocompletion import autocompletion_request

from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, \
    QLabel, QFormLayout, QGridLayout, QApplication, QComboBox, \
    QVBoxLayout, QHBoxLayout, QCompleter, QSpacerItem, QSizePolicy, QMessageBox
from PyQt5.QtCore import Qt, QStringListModel, QThread, pyqtSlot, pyqtSignal


class NFZAsyncRequest(QThread):
    def __init__(self, parent):
        super().__init__(parent)


    def run(self):
        response = autocompletion_request(self.parent().text(), self.parent().state, self.parent().request_name)
        self.parent().model.setStringList(response)


class NFZRequestLineEdit(QLineEdit):
    def __init__(self, request_name):
        super().__init__()
        self.request_name = request_name
        self.async_caller = NFZAsyncRequest(self)

        self.completer = QCompleter(self)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.completer.setFilterMode(Qt.MatchContains)
        self.model = QStringListModel()
        self.completer.setModel(self.model)
        self.setCompleter(self.completer)

        self.textEdited.connect(self.request_auto_completion)


    @property
    def state(self):
        state = self.parent().state_input.currentText()
        if state not in STATES:
            return ''
        else:
            return state


    def request_auto_completion(self):
        if len(self.text()) >= 3:
            if self.async_caller.isRunning():
                self.async_caller.exit()

            self.async_caller = NFZAsyncRequest(self)
            self.async_caller.finished.connect(self.completer.complete)
            self.async_caller.start()
        elif len(self.text()) == 0:
            self.model.setStringList([])


class AddNFZVisit(QDialog):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.data = {}

        # Inputy
        self.case_input = QComboBox(self)
        self.case_input.addItems(['stabliny', 'pilny'])
        self.service_input = NFZRequestLineEdit('service_name')
        self.state_input = QComboBox(self)
        self.state_input.addItem('- wszystkie -')
        self.state_input.addItems(STATES)
        self.locality_input = NFZRequestLineEdit('locality')

        self.provider_input = NFZRequestLineEdit('provider')
        self.place_input = NFZRequestLineEdit('place')
        self.street_input = NFZRequestLineEdit('street')

        # Buttony
        self.cancel = QPushButton('Anuluj', self)
        self.ok = QPushButton('Dodaj', self)
        self.cancel.clicked.connect(self.reject)
        self.ok.clicked.connect(self.form_data)

        # Layouty
        form = QFormLayout()
        form.addRow('Przypadek', self.case_input)
        form.addRow('Nazwa świadczenia*', self.service_input)
        form.addRow('Województwo', self.state_input)
        form.addRow('Miejscowość', self.locality_input)
        form.addRow('Szpital/przychodnia', self.provider_input)
        form.addRow('Miejsce udzielania świadczeń', self.place_input)
        form.addRow('Ulica', self.street_input)

        hbox = QHBoxLayout()
        hbox.addStretch()
        hbox.addWidget(self.cancel)
        hbox.addWidget(self.ok)

        vbox = QVBoxLayout(self)
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        vbox.addStretch()

        self.setLayout(vbox)
        self.setWindowTitle('Dodaj wizytę w NFZ')
        self.setModal(True)
        self.show()
        self.setFixedSize(550, self.height())


    def form_data(self):
        if len(self.service_input.text()) == 0:
            QMessageBox.critical(self, 'Brak wymaganych danych', 'Proszę podać nazwę świadczenia', QMessageBox.Ok)
            return

        self.data['case'] = self.case_input.currentText()
        self.data['service_name'] = self.service_input.text()
        self.data['state'] = self.state_input.currentText()
        self.data['locality'] = self.locality_input.text()
        self.data['provider'] = self.provider_input.text()
        self.data['place'] = self.place_input.text()
        self.data['street'] = self.street_input.text()
        self.accept()


if __name__ == '__main__':
    app = QApplication([])
    anv = AddNFZVisit()
    app.exec_()
