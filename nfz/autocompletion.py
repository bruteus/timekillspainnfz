from urllib.request import urlopen
from urllib.parse import quote
from json import loads
from util import get_state_id, BASE_URL

DICT_PREFIX = {
    'locality': 'Localities',
    'place': 'Places',
    'provider': 'Providers',
    'service_name': 'Services',
    'street': 'Streets'
}
QUERY_DEFAULTS = 'page=1&limit=10'


def autocompletion_request(base, state, parameter):
    """Wysyła zapytanie na serwer NFZ w celu pobrania listy zawierającej
    dostępne dane do autokompletowania

    Parametry:
    → `base` - string o długości co najmniej 3 znaków, który stanowi początek
    wyrażenia wpisanego przez użytkownika do pola formularza
    → `state` - województwo, które użytkownik wybrał w formularzu
    → `parameter` - wartość określająca, z którego słownika mają zostać pobrane
    dane do autokompletowania; powinna być wybrana z kluczy `DICT_PREFIX`

    Zwraca listę jednowymiarową z danymi pobranymi z serwera.
    """
    # Sprawdzanie poprawności parametrów
    if parameter not in DICT_PREFIX.keys():
        raise ValueError('`parameter` should be one of: {}'.format(DICT_PREFIX.keys()))
    if type(base) != str or type(state) != str:
        raise TypeError('Non-string paramter found')
    if len(base) < 3:
        raise ValueError('`base` shouldn\'t have less than 3 characters')

    dict_name = DICT_PREFIX[parameter] + 'Dictionary'
    url = '{}{}?name={}&state={}&{}'.format(
        BASE_URL, dict_name, quote(base),
        quote(get_state_id(state)), QUERY_DEFAULTS
    )

    return loads(urlopen(url).read().decode())['results']
