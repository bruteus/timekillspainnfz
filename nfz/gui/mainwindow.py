from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from addnfzdialog import AddNFZVisit
from preferencesdialog import NFZPreferences
from central import Central, VisitModel
from json import load, dump, dumps
import os.path

class NFZMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.create_actions()
        self.create_menu()
        self.init_ui()


    def init_ui(self):
        self.central = Central(self)
        self.model = VisitModel()
        self.central.setModel(self.model)

        self.setCentralWidget(self.central)

        self.load_data()

        self.setWindowTitle('NFZ - Czas leczy rany')
        self.setMinimumSize(1024, 480)
        self.show()


    def load_data(self):
        visits = None
        if not os.path.exists('data.json'):
            return
        with open('data.json', 'r') as f:
            if f.read() == '':
                return
            else:
                f.seek(0)
                visits = load(f)

        for service_name, params in visits.items():
            visit_info = params
            visit_info['service_name'] = service_name
            self.model.appendRow(visit_info)


    def create_menu(self):
        file_menu = self.menuBar().addMenu('Plik')
        file_menu.addAction(self.save)
        file_menu.addAction(self.leave)

        appointments__menu = self.menuBar().addMenu('Wizyty')

        add_appointment_menu = QMenu('Dodaj wizytę...', appointments__menu)
        add_appointment_menu.addAction(self.add_nfz)
        add_appointment_menu.addAction(self.add_medicover)
        add_appointment_menu.addAction(self.add_luxmed)
        appointments__menu.addAction(self.org_appointments)
        appointments__menu.addMenu(add_appointment_menu)

        options_menu = self.menuBar().addMenu('Opcje')
        options_menu.addAction(self.about)
        options_menu.addAction(self.preferences)


    def create_actions(self):
        self.save = QAction('Zapisz', self)
        self.leave = QAction('Wyjdź', self)
        self.save.triggered.connect(self.save_action)
        self.leave.triggered.connect(self.leave_action)

        self.add_nfz = QAction('NFZ', self)
        self.add_medicover = QAction('Medicover', self)
        self.add_luxmed = QAction('LuxMed', self)
        self.org_appointments = QAction('Zarządzaj wizytami', self)
        self.add_nfz.triggered.connect(self.add_nfz_action)
        self.add_medicover.triggered.connect(self.add_medicover_action)
        self.add_luxmed.triggered.connect(self.add_luxmed_action)
        self.org_appointments.triggered.connect(self.org_appointments_action)

        self.about = QAction('O programie', self)
        self.org_appointments.triggered.connect(self.org_appointments_action)
        self.preferences = QAction('Ustawienia', self)
        self.about.triggered.connect(self.about_action)
        self.preferences.triggered.connect(self.preferences_action)


    def save_action(self):
        with open('data.json', 'w') as f:
            data = {}
            for row in range(self.model.rowCount()):
                visit_info = self.model.item(row).data()
                service_name = visit_info.pop('service_name')
                data[service_name] = visit_info
            dump(data, f, ensure_ascii = False, indent = 2)


    def maybe_save(self):
        response = QMessageBox.question(self, 'Pytanie', 'Czy chcesz zapisać wizyty?', QMessageBox.Yes | QMessageBox.No)
        if response == QMessageBox.Yes:
            self.save_action()


    def leave_action(self):
        program_data = {}
        for row in range(self.model.rowCount()):
            visit_info = self.model.item(row).data()
            service_name = visit_info.pop('service_name')
            program_data[service_name] = visit_info

        if os.path.exists('data.json'):
            with open('data.json', 'r') as f:
                if f.read() == '':
                    f.seek(0)
                    if program_data != {}:
                        self.maybe_save()
                else:
                    f.seek(0)
                    if program_data != load(f):
                        self.maybe_save()
        elif program_data != {}:
            self.maybe_save()

        self.close()


    def add_nfz_action(self):
        nfz_visit = AddNFZVisit(self)
        if nfz_visit.exec_():
            self.model.appendRow(nfz_visit.data)
            print(nfz_visit.data)


    def add_medicover_action(self):
        QMessageBox.information(self, 'Notification', 'Not implemented', QMessageBox.Ok)


    def add_luxmed_action(self):
        QMessageBox.information(self, 'Notification', 'Not implemented', QMessageBox.Ok)


    def org_appointments_action(self):
        QMessageBox.information(self, 'Notification', 'Not implemented', QMessageBox.Ok)


    def about_action(self):
        QMessageBox.information(self, 'Notification', 'Not implemented', QMessageBox.Ok)


    def preferences_action(self):
        preferences = NFZPreferences(self)
        if preferences.exec_():
            print(preferences.data)


app = QApplication([])
nfzmw = NFZMainWindow()
app.exec_()
