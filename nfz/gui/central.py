import sys
sys.path.append('..')
from request import NFZRequest

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class VisitModel(QStandardItemModel):
    def __init__(self):
        super().__init__()


    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self.itemFromIndex(index).data()['service_name']

        return super().data(index, role)


    def appendRow(self, visit_info):
        item = QStandardItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        item.setData(visit_info)
        super().appendRow(item)


    def refresh_results(self, index):
        item = self.itemFromIndex(index)
        new_data = item.data()
        request = NFZRequest.from_dict(new_data)
        request.send_request()
        request.extract_data()
        new_data['results'] = request.results_json
        item.setData(new_data)


class Central(QWidget):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.visit_list = QListView(self)
        self.visit_report = QTextEdit(self)

        self.visit_report.setReadOnly(True)

        self.splitter = QSplitter(self)
        self.splitter.addWidget(self.visit_list)
        self.splitter.addWidget(self.visit_report)

        vbox = QVBoxLayout(self)
        vbox.addWidget(self.splitter)
        self.setLayout(vbox)

        self.show()


    def setModel(self, model):
        self.visit_list_model = model
        self.visit_list.setModel(model)
        self.visit_list.selectionModel().currentChanged.connect(self.update_report)


    @pyqtSlot(QModelIndex, QModelIndex)
    def update_report(self, current, old):
        data = self.visit_list_model.itemFromIndex(current).data()
        text = '<h1>Badanie: <u>{}</u></h1>'.format(data['service_name'])
        text += '<ul type="square">'
        ins_list_el = lambda k, v: '<li><b>{}</b> - {}</li>'.format(k, v)
        items = [
            ['Przypadek', data['case']],
            ['Województwo', data['state']],
            ['Miejscowość', data['locality']],
            ['Szpital/przychodnia', data['place']],
            ['Ulica', data['street']]
        ]

        for element in items:
            if element[1] != '':
                text += ins_list_el(element[0], element[1])
        text += '</ul>'
        self.visit_report.setHtml(text)

        self.visit_list_model.refresh_results(current)


    def form_visits_html(self, index):
        visits = self.visit_list_model.itemFromIndex(index).data()['results']
